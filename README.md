# Read Write Driver

A simple driver that provides read and write function. It can write user application data into its internal buffer and also provides the contents of this buffer on read.
