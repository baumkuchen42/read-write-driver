#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/fs.h>

char* DEVICE_PATH = "/dev/readwrite";
size_t nr_bytes_to_write = 10;
char* buffer[11]; // one byte more than number of bytes to write,
		  // to also include string terminator which is not given 
		  // by reading console input raw into buffer

void write_to_driver(int device_handle, off_t offset) {
	printf("starting to write to driver\n");
	int write_return_code;
	write_return_code = write(device_handle, buffer, nr_bytes_to_write);
	printf("written\n");
	
	if (write_return_code == nr_bytes_to_write) {
		printf("Successful write.\n");
	}
	else if (write_return_code == 0) {
		printf("Didn't write anything.\n");
	}
	else if (write_return_code < nr_bytes_to_write) {
		printf("Partial write.\n");
		offset += write_return_code;
		write_to_driver(device_handle, offset);
	}
	else if (write_return_code < 0) {
		printf("An error occured. Error code: %i\n", write_return_code);
	}
}

int main (int argc, char **argv) {
	int device_handle = open(DEVICE_PATH, O_RDWR);
	printf("Opened driver, file descriptor is: %i\n", device_handle);
	
	read(STDIN_FILENO, buffer, nr_bytes_to_write); // read user input
	buffer[nr_bytes_to_write + 1] = "\0"; // to zero-terminate string
	printf("You wrote %s\n", buffer);
	
	write_to_driver(device_handle, 0);
	
	close(device_handle);
	printf("Closed driver\n");
}
