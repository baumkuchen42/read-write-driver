ifneq ($(KERNELRELEASE),)
obj-m := read_write_driver.o
else
KDIR := /lib/modules/$(shell uname -r)/build
PWD := $(shell pwd)
default:
	$(MAKE) -C $(KDIR) M=$(PWD) modules
endif
