#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/fs.h>

char* DEVICE_PATH = "/dev/readwrite";
size_t nr_bytes_to_request = 100;
char* buffer[100]; // same size as number of requested bytes

void read_from_driver(int device_handle, off_t offset) {
	int read_return_code;
	read_return_code = read(device_handle, buffer, nr_bytes_to_request);
	
	if (read_return_code == nr_bytes_to_request) {
		printf("Successful read. Read content:\n %s", buffer);
	}
	else if (read_return_code == 0) {
		printf("Didn't read anything.");
	}
	else if (read_return_code < nr_bytes_to_request) {
		printf("Partial read. Read content:\n %s", buffer);
		offset += read_return_code;
		read_from_driver(device_handle, offset);
	}
	else if (read_return_code < 0) {
		printf("An error occured. Error code: %i", read_return_code);
	}
}

int main (int argc, char **argv) {
	int device_handle = open(DEVICE_PATH, O_RDONLY);
	printf("Opened driver, file descriptor is: %i\n", device_handle);
	
	read_from_driver(device_handle, 0);
	
	close(device_handle);
	
	printf("Closed driver\n");
}
