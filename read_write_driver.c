#include <linux/module.h>
#include <linux/init.h> // need this header file for the key words __init and __exit which help the kernel to better manage storage resources
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>

#define STORAGE_SIZE 1024
#define WRITE_POSSIBLE (STORAGE_SIZE != 0)


static dev_t device_nr; // the variable where the device number will be written to
static char driver_name[] = "readwrite";
static struct cdev *driver_object;
struct class *driver_class;
static struct device *dev;

static char* driver_storage[STORAGE_SIZE];
static wait_queue_head_t wq_read, wq_write;

/*
* Application calls open, then kernel checks, if access allowed, then calls this function.
* This function can also be left out, then access to the driver is always allowed,
* as if the function would only return 0 (like in this example)
*
* @param dev_file contains elements to specify the device file like its owner and access rights
* @param driver_instance contains elements that specify the calling driver instance: access mode, blocking or non-blocking
*/
static int driver_open(struct inode *dev_file, struct file *driver_instance) {
	pr_info("%s: opened", driver_name);
	return 0;
}

static int driver_close(struct inode *dev_file, struct file *driver_instance) {
	pr_info("%s: closed", driver_name);
	return 0;
}

/*
* This read function copies the device info that we set at the top of this code
* to the user, the driver instance this read comes from. This instance is given by the arg
* user, marked by __user to tell the kernel it's a potentially faulty userspace pointer.
* Count is the count of bytes that the instance wants to read.
* The last arg is the offset of the type "long offset type". It points to the part that
* should be read from and gets refreshed with each progression inside the space.
*/
static ssize_t driver_read (struct file *instanz, char __user *user, size_t count, loff_t *offset) {
	unsigned long not_copied, to_copy;
	
	// either copies whole info or copies only as much as the caller instance can take 
	to_copy = min(count, STORAGE_SIZE);
	
	// not_copied returns the number of not yet copied bytes,
	// tells us, how much we still need to copy in the next call of this function
	not_copied = copy_to_user(user, driver_storage, to_copy);
	
	// refresh offset with how much we progressed in this turn
	*offset += to_copy - not_copied;
	
	// returns number of yet to copy bytes so the calling instance can get them
	// by calling read again, until it returns 0
	return to_copy - not_copied;
}

ssize_t driver_write(struct file *instance, const char __user *buffer, size_t max_bytes_to_write, loff_t *offset) {
	size_t to_copy, not_copied;
	
	/* Hardware/Treiber ist nicht bereit, die Daten zu schreiben
	* und der Zugriff erfolgt im NON_BLOCKING-Mode
	*/
	pr_alert("%s: before checking if nonblocking", driver_name);
	if (instance == NULL) {
		pr_err("%s: Caller instance is NULL", driver_name);
		return -42;
	}
	if (!WRITE_POSSIBLE && (instance->f_flags & O_NONBLOCK)) {
		return -EAGAIN;
	}

	pr_alert("%s: before sending caller to sleep", driver_name);
	/* sends caller to sleep in 1st arg queue until event (2nd arg) is true */
	if (wait_event_interruptible(wq_write, WRITE_POSSIBLE)) {
		return -ERESTARTSYS;
	}
	pr_alert("%s: before checking how much to copy", driver_name);
	to_copy = min((size_t) STORAGE_SIZE, max_bytes_to_write);
	pr_alert("%s: before copy from user", driver_name);
	not_copied = copy_from_user(&driver_storage, buffer, to_copy); // copies data to kernel memory
	pr_alert("%s: before atomic sub and offset refreshing", driver_name);
	// atomic_sub(to_copy - not_copied, STORAGE_SIZE); // not sure what this does
	*offset += to_copy - not_copied;
	return to_copy - not_copied;
}

/* Provides the driver methods which were defined above */
static struct file_operations fops = {
	.owner = THIS_MODULE,
	.write = driver_write,
	.read = driver_read,
	.open = driver_open,
	.release = driver_close,
};


//////// driver init ///////////////////////////////////////////////////////

static int create_and_add_driver_object(void) {
	driver_object = cdev_alloc(); /* Anmeldeobjekt reservieren */
	
	if (driver_object == NULL) {
		unregister_chrdev_region(device_nr, 1);
		pr_err("%s: couldn't allocate driver object\n", driver_name);
		return -EIO;
	}
		
	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;
	
	if (cdev_add(driver_object, device_nr, 1) != 0) {
		kobject_put(&driver_object->kobj); // dereferences object
		unregister_chrdev_region(device_nr, 1);
		pr_err("%s: could not add device to system", driver_name);
		return -EIO;
	}
	return 0;
}

/* Create class for sysfs entry */
static int create_driver_class(void) {
	driver_class = class_create(THIS_MODULE, driver_name);
	if (IS_ERR(driver_class)) {
		pr_err("%s: no udev support\n", driver_name);
		kobject_put(&driver_object->kobj); // dereferences object
		unregister_chrdev_region(device_nr, 1);
		return -EIO;
	}
	return 0;
}

/* 
* Gives udev the info to create the sysfs entry
* creates a device with class, parent, a device nr of type dev_t 
* (data type for device numbers), data for callbacks and string for device name
*/
static int create_driver_entry(void) {
	dev = device_create(driver_class, NULL, device_nr, NULL, "%s", driver_name);
	if (IS_ERR(dev)) {
		pr_err("%s: device_create failed\n", driver_name);
		class_destroy(driver_class);
		unregister_chrdev_region(device_nr, 1);
		return -EIO;
	}
	return 0;
}

static int create_dev(void) {
	if (
		create_and_add_driver_object() == 0
		&& create_driver_class() == 0
		&& create_driver_entry() == 0
	) {
		return 0;
	}
	return -EIO;
}

/* Init function. Name is arbitrary. __init is just a hint, can be left out */
static int __init mod_init(void) {
	init_waitqueue_head(&wq_read);
	init_waitqueue_head(&wq_write);
	if (
		alloc_chrdev_region(&device_nr, 0, 1, driver_name) == 0
		&& create_dev() == 0
	) {
		pr_info("%s: successfully initialised\n", driver_name);
		return 0;
	}
	pr_err("%s: failed to initialise\n", driver_name);
	return -EIO;
}

/* Also arbitrary name */
static void __exit mod_exit(void) {
	// Delete sysfs entry and also the device class
	device_destroy(driver_class, device_nr);
	class_destroy(driver_class);
	
	// Unregister driver
	cdev_del(driver_object);
	unregister_chrdev_region(device_nr, 1);
	pr_info("%s: unregistered\n", driver_name);
}

module_init(mod_init); // a makro that connects the freely chosen init function name to the actual init function name, for module drivers it's init_module
module_exit(mod_exit); // same but for exit, for module drivers it's cleanup_module

/* Metainformation */
MODULE_AUTHOR("Uta Lemke");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("A simple driver that only supports open and close.");
MODULE_SUPPORTED_DEVICE("none");
